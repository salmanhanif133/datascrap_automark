const fs = require('fs');
const fetch = require('node-fetch');
const cheerio = require('cheerio');
const url = 'https://graphql.automark.pk/v1/graphql';
const graphbody = (address, contact_person, name, phone_number, city) => `mutation MyMutation {
  insert_dealer_one(object: {
    address: "${address}", 
    contact_person: "${contact_person}", 
    name: "${name}", 
    phone_number: "${phone_number}",
    city: "${city}"
  }) {
    id
  }
}`;
const cities = ["Sukkur", "Karachi", "Hub ", "Pano Aquil", "Khairpur Mirus", "Quetta", "Ghotki", "Kandh kot", "Mastung", "Noshki", "Mehrabpur", "Turbat", "Gambat", "Mirpur Mathelo", "Moro", "Kot Banglo", "Zaffarabad", "Kashmor", "Khuzdar", "Pakka Chang", "Thari Mir Wah", "Daherki", "Hyderabad", "Makli", "Badin", "Khipro", "Mehar"];

(async () => {
let c = 1;
  for (let i = 0; i < cities.length; i++) {
    const city = cities[i]
    // let dealersArray = []
    let body = null;
    try {
      const res = await fetch(`http://piranigroup.com.pk/wp-content/themes/sp-wordpress/DisplayDealer.php?city=${city}`);
      body = await res.text();
      console.log(`${res.status} ${city}`)
    } catch (e) {
      console.log(e)
    }
    const $ = cheerio.load(body);
    for (let i = 0; i < ($("tbody").find("tr").length); i++) {
      let content = ($('tr').slice(i, i + 1).find('p').html().replace("<br>", ",").split(","))
      let dealer = content.shift(); //remove first element and return
      let contact = content.pop().trim(); //remove last element and return
      contact = contact.trim().split(" ");
      let number = "+" + contact.pop().trim(); //remove last element and return
      let name = contact.join(" ").trim();
      let address = content.join().trim()



      const postRequest = await fetch(url, {
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify({
          query: graphbody(address, name, dealer, number, city)
        })
      });

      if (postRequest.status === 200) {
        console.log(`${name}      ${c}`)
        c++;
      } else {
        console.log("Error")
      }
    }
  }
  console.log(c)
  // const allDealers = dealersArray;
  // const filename = `${city}.json`;
  // fs.writeFileSync(filename, JSON.stringify(allDealers, null, 2), 'utf8');
})()