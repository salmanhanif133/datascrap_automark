
const url = 'https://graphql.automark.pk/v1/graphql';

const body = (address, contact_person, name, phone_number, city) => `mutation MyMutation {
  insert_dealer_one(object: {
    address: "${address}", 
    contact_person: "${contact_person}", 
    name: "${name}", 
    phone_number: "${phone_number}",
    city: "${city}"
  }) {
    id
  }
}`;

const postRequest = await fetch(url, {
  headers: {
    'Content-Type': 'application/json'
  },
  method: 'post',
  body: JSON.stringify({
    query: body('address', '', 'Salman', '13123', 'Karachi')
  })
});

if (postRequest.status === 200) {
  console.log('success')
}