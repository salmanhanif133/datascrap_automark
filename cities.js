const cities = [
  { city: "CHOBARA", value: "551" },
  { city: "18 Hazari", value: "231" },
  { city: "49 TAIL", value: "443" },
  { city: "A.P.Chatha", value: "314" },
  { city: "ABBASPUR (A.K)", value: "614" },
  { city: "ABBOTABAD", value: "390" },
  { city: "Abbottabad", value: "194" },
  { city: "Abdul Hakim", value: "76" },
  { city: "Ada Sherinwala", value: "516" },
  { city: "ADAH KAMAL PUR", value: "539" },
  { city: "ADAM KAY CHEEMA", value: "535" },
  { city: "ADDA 12 MEEL", value: "478" },
  { city: "ADDA BADYANA", value: "430" },
  { city: "Adda Chawant", value: "527" },
  { city: "ADDA CHUND BHARWANA", value: "598" },
  { city: "ADDA DAWAKRI", value: "595" },
  { city: "ADDA HAFIZWALA", value: "451" },
  { city: "ADDA IQBAL ABAD", value: "714" },
  { city: "ADDA JAHAN KHAN", value: "450" },
  { city: "Adda Kamand", value: "397" },
  { city: "ADDA KERANWALA", value: "564" },
  { city: "ADDA KHAYE", value: "556" },
  { city: "ADDA LAAR", value: "660" },
  { city: "ADDA LOTHUR", value: "668" },
  { city: "ADDA NAE WALA", value: "468" },
  { city: "ADDA NAWA LAHORE", value: "380" },
  { city: "ADDA PANUWAN", value: "550" },
  { city: "ADDA PEER ABDUR REHMAN", value: "601" },
  { city: "Adda Peerowal", value: "692" },
  { city: "ADDA QADIRABAD COLONY", value: "560" },
  { city: "ADDA RASOOL PUR BHLIAN", value: "426" },
  { city: "ADDA SHAH JIVNA", value: "592" },
  { city: "ADDA YOUSAF ABAD", value: "713" },
  { city: "ADDAH", value: "554" },
  { city: "ADIL PUR", value: "709" },
  { city: "AHMAD PUR EAST", value: "694" },
  { city: "AHMAD PUR LAMMA", value: "715" },
  { city: "AHMAD PUR SIAL", value: "381" },
  { city: "AHMADPUR EAST", value: "406" },
  { city: "AHMEDABAD (KARAK)", value: "458" },
  { city: "Ahmedpur East", value: "344" },
  { city: "Ahsan Pur", value: "485" },
  { city: "Ali Pur", value: "149" },
  { city: "ALI PUR CHATHA", value: "544" },
  { city: "ALI PUR SYEDAN", value: "437" },
  { city: "ALI WAHAN", value: "728" },
  { city: "Alipur", value: "658" },
  { city: "AMEER ABAD", value: "456" },
  { city: "AMEER PUR STATION", value: "677" },
  { city: "AMIN PUR BANGLA", value: "593" },
  { city: "Aminpur Bangla", value: "229" },
  { city: "Arif Wala", value: "422" },
  { city: "Arifwala", value: "2" },
  { city: "ATTHMUQAM", value: "639" },
  { city: "ATTOCK", value: "31" },
  { city: "AWAN ABAD (A.K)", value: "624" },
  { city: "Awaran", value: "748" },
  { city: "Badin", value: "102" },
  { city: "Badyana", value: "188" },
  { city: "Bagh", value: "88" },
  { city: "BAGH (A.K)", value: "467" },
  { city: "Bahawalnagar", value: "140" },
  { city: "Bahawalpur", value: "54" },
  { city: "BAHAWALUR", value: "490" },
  { city: "BAJOUR", value: "615" },
  { city: "BALOCH (A.K)", value: "466" },
  { city: "Bandhi", value: "111" },
  { city: "BANGALA MORR", value: "678" },
  { city: "BANGLA DHEGAN", value: "675" },
  { city: "BANGLA MANTHAR", value: "705" },
  { city: "BANGLA YATEEM WALA", value: "686" },
  { city: "BANNU", value: "27" },
  { city: "BARA GHAR", value: "425" },
  { city: "BARNALA (A.K)", value: "386" },
  { city: "Baseer Pur", value: "526" },
  { city: "Baseera", value: "683" },
  { city: "Baseerpur", value: "363" },
  { city: "Basti Malook", value: "305" },
  { city: "BATAGARAM", value: "394" },
  { city: "BATTAGRAM", value: "19" },
  { city: "BATTAL", value: "457" },
  { city: "BEVEL", value: "648" },
  { city: "BHAGTANWALA", value: "383" },
  { city: "BHAGWAAR", value: "647" },
  { city: "Bhakkar", value: "225" },
  { city: "Bhalwal", value: "234" },
  { city: "BHAN SAEEDABAD", value: "501" },
  { city: "BHAWALNAGAR", value: "679" },
  { city: "Bhawana", value: "341" },
  { city: "Bheira", value: "235" },
  { city: "Bhimber", value: "326" },
  { city: "BHIMBER (A.K)", value: "393" },
  { city: "BHIRI MORE", value: "540" },
  { city: "BHIRYA ROAD", value: "412" },
  { city: "BHITSHAH", value: "725" },
  { city: "BHOPAL WALA", value: "557" },
  { city: "BOSAL", value: "563" },
  { city: "BUCHEKI", value: "543" },
  { city: "BUDLA SANTH", value: "684" },
  { city: "BUFFA DORAHA", value: "651" },
  { city: "BUNER GAGRA", value: "28" },
  { city: "Burewala", value: "47" },
  { city: "C.S.Shaheed", value: "307" },
  { city: "CHAK 5 KLAN", value: "373" },
  { city: "CHAK HOTIYANA", value: "656" },
  { city: "CHAK JHUMRA", value: "377" },
  { city: "Chak swari", value: "208" },
  { city: "CHAK-177 MURAD", value: "674" },
  { city: "Chakdara", value: "214" },
  { city: "CHAKSWARI (A.K)", value: "628" },
  { city: "CHAKWAL", value: "22" },
  { city: "CHAMAN", value: "65" },
  { city: "CHAMB MORR", value: "471" },
  { city: "CHAMBAR", value: "414" },
  { city: "CHAMBARH", value: "739" },
  { city: "CHANDAR NAGAR", value: "530" },
  { city: "Changa Manga", value: "419" },
  { city: "Chani Goth", value: "53" },
  { city: "Charsadda", value: "329" },
  { city: "CHASHMA", value: "599" },
  { city: "Chawinda", value: "189" },
  { city: "Chichawatni", value: "131" },
  { city: "Chillas", value: "195" },
  { city: "Chiniot", value: "90" },
  { city: "Chishtian", value: "137" },
  { city: "CHITIANA STATION", value: "612" },
  { city: "CHITRAL", value: "17" },
  { city: "CHOA SYEDAN SHAH", value: "650" },
  { city: "Chobara", value: "310" },
  { city: "CHOBARA", value: "367" },
  { city: "Chohar Jamali", value: "286" },
  { city: "CHOKI (A.K)", value: "634" },
  { city: "Chonawala", value: "241" },
  { city: "Chor", value: "259" },
  { city: "Chore Jamali", value: "746" },
  { city: "Choti Zerin", value: "72" },
  { city: "CHOTTI ZARIN", value: "480" },
  { city: "Chowk Azam", value: "311" },
  { city: "Chowk Bhata", value: "238" },
  { city: "CHOWK BHATTA", value: "496" },
  { city: "CHOWK MAHI", value: "498" },
  { city: "Chowk Qurashi", value: "657" },
  { city: "CHOWK SAWETRA", value: "497" },
  { city: "Chowk Serwer Shaheed", value: "664" },
  { city: "CHUND BHARWALA", value: "607" },
  { city: "Chunian", value: "293" },
  { city: "D.G Khan", value: "144" },
  { city: "D.G.Khan", value: "73" },
  { city: "D.I.KHAN", value: "392" },
  { city: "DABLIWALA", value: "565" },
  { city: "Dad Fatyana", value: "515" },
  { city: "Dadu", value: "349" },
  { city: "Dadyal", value: "327" },
  { city: "DADYAL (A.K)", value: "388" },
  { city: "DAGAR", value: "632" },
  { city: "Daharki", value: "100" },
  { city: "DAHRANWALA", value: "401" },
  { city: "DAJIL", value: "691" },
  { city: "Darya Khan", value: "334" },
  { city: "Daska", value: "190" },
  { city: "Dehranwala", value: "296" },
  { city: "Denanath", value: "520" },
  { city: "Depal Pur", value: "522" },
  { city: "Depalpur", value: "132" },
  { city: "Deplapur", value: "356" },
  { city: "Dera Allah Yar", value: "105" },
  { city: "Dera Ismail Khan", value: "200" },
  { city: "Dera Murad Jamali", value: "106" },
  { city: "DERYANWALA", value: "572" },
  { city: "DG KHAN", value: "395" },
  { city: "DHAMTAL", value: "364" },
  { city: "DHANOT", value: "470" },
  { city: "Dhing Shah", value: "529" },
  { city: "DHOKE AMB", value: "616" },
  { city: "Digri", value: "253" },
  { city: "Dijkot", value: "336" },
  { city: "Dina", value: "328" },
  { city: "Dinga", value: "318" },
  { city: "Dokota", value: "685" },
  { city: "DOKRI", value: "415" },
  { city: "Doltala", value: "220" },
  { city: "DOMAIL (BANNU)", value: "465" },
  { city: "Donga Bonga", value: "69" },
  { city: "DOUR", value: "411" },
  { city: "Dukki", value: "352" },
  { city: "Duniapur", value: "70" },
  { city: "DUNIYAPUR", value: "487" },
  { city: "Elah Abad", value: "423" },
  { city: "Ellahabad", value: "126" },
  { city: "Essa", value: "177" },
  { city: "Faisalabad", value: "36" },
  { city: "FAISALABAD", value: "378" },
  { city: "Faqeerwali", value: "297" },
  { city: "FAQIR WALI", value: "477" },
  { city: "FAQIRWALI", value: "654" },
  { city: "FAROOQA", value: "610" },
  { city: "Farooqabad", value: "92" },
  { city: "Fateh Jang", value: "197" },
  { city: "Fatehpur", value: "156" },
  { city: "Fatteh Pur", value: "662" },
  { city: "FATTEH PUR KAMAL", value: "717" },
  { city: "Fattehpur", value: "479" },
  { city: "Fazil Pur", value: "301" },
  { city: "Fazilpur", value: "48" },
  { city: "Feroza", value: "245" },
  { city: "Ferozwala", value: "176" },
  { city: "Fort Abbas", value: "298" },
  { city: "FORT ABBASS", value: "486" },
  { city: "Gaggo Mandi", value: "398" },
  { city: "Gakuch", value: "196" },
  { city: "GALEWAL", value: "681" },
  { city: "Gambat", value: "272" },
  { city: "Gamber", value: "133" },
  { city: "GAR FATTEH SHAH", value: "584" },
  { city: "Garh More", value: "338" },
  { city: "Garha More", value: "159" },
  { city: "GARHI DUPATTA (A.K)", value: "454" },
  { city: "Gawadar", value: "279" },
  { city: "Gelewal", value: "306" },
  { city: "GHAGH CHOWK", value: "440" },
  { city: "Ghakhar", value: "164" },
  { city: "GHAKKAR MANDI", value: "366" },
  { city: "GHAR MAHARAJA", value: "597" },
  { city: "GHAR MORE", value: "609" },
  { city: "Gharo", value: "743" },
  { city: "GHIZER", value: "631" },
  { city: "GHOINKI", value: "559" },
  { city: "Ghotki", value: "347" },
  { city: "GILGIT", value: "29" },
  { city: "GOJRA", value: "41" },
  { city: "Golarchi", value: "59" },
  { city: "GOTH MACHI", value: "720" },
  { city: "GUDDU", value: "404" },
  { city: "Gujar Khan", value: "87" },
  { city: "Gujranwala", value: "10" },
  { city: "Gujrat", value: "8" },
  { city: "GULAB LAGHARI", value: "730" },
  { city: "GULABAD", value: "649" },
  { city: "GUNAH KLAN", value: "569" },
  { city: "Gwadar", value: "506" },
  { city: "HAALA", value: "503" },
  { city: "Habibabad", value: "517" },
  { city: "Hafizabad", value: "11" },
  { city: "HAIDERABAD", value: "578" },
  { city: "HAJI PUR", value: "665" },
  { city: "Hajira", value: "224" },
  { city: "HAJIRA (A.K)", value: "623" },
  { city: "Hala", value: "264" },
  { city: "HALAL PUR", value: "582" },
  { city: "Hangu", value: "83" },
  { city: "Harappa", value: "67" },
  { city: "HARARH", value: "561" },
  { city: "HARIPUR", value: "15" },
  { city: "Harniawala", value: "521" },
  { city: "HARNOLI", value: "589" },
  { city: "Haroonabad", value: "299" },
  { city: "Harrapa", value: "358" },
  { city: "Harrapa City", value: "511" },
  { city: "Harrapa Station", value: "512" },
  { city: "Hasilpur", value: "242" },
  { city: "HASOO BALAIL", value: "590" },
  { city: "HASSAN ABDAAL", value: "621" },
  { city: "Haveilakha", value: "134" },
  { city: "Haveli Lakha", value: "523" },
  { city: "HAVELLIAN", value: "30" },
  { city: "HAZRO", value: "32" },
  { city: "HEAD FAQIRIAN", value: "534" },
  { city: "Head Marala", value: "191" },
  { city: "Headrajkan", value: "239" },
  { city: "HINGORJA", value: "731" },
  { city: "Hub", value: "280" },
  { city: "Hujra Shah Muqeem", value: "4" },
  { city: "Hujra.S.Muqem", value: "420" },
  { city: "Hyderabad", value: "58" },
  { city: "Hyderabad", value: "103" },
  { city: "Iqbal Nagar", value: "514" },
  { city: "ISLAM GARH (A.K)", value: "641" },
  { city: "ISLAMABAD", value: "21" },
  { city: "Islamgarh", value: "209" },
  { city: "JABBAR", value: "643" },
  { city: "Jacobabad", value: "260" },
  { city: "JADU WAHAN", value: "737" },
  { city: "Jahangira", value: "215" },
  { city: "Jahanian", value: "77" },
  { city: "JAJJA ABBASIA", value: "693" },
  { city: "JALAL PUR BHATTIAN", value: "531" },
  { city: "JALALA", value: "617" },
  { city: "Jalalpur", value: "146" },
  { city: "Jalalpur Jattan", value: "319" },
  { city: "JALALPUR SOBTIAN", value: "424" },
  { city: "JAM PUR", value: "403" },
  { city: "Jamal Din Wali", value: "246" },
  { city: "JAMAL PUR", value: "704" },
  { city: "JAMALDIN WALI", value: "718" },
  { city: "Jampur", value: "302" },
  { city: "JAMSHORO", value: "733" },
  { city: "Jand", value: "80" },
  { city: "JANPUR SADAAT", value: "707" },
  { city: "Jaranwala", value: "35" },
  { city: "JARRANWALA", value: "382" },
  { city: "Jatlan", value: "84" },
  { city: "Jatoi", value: "147" },
  { city: "JAUHARABAD", value: "384" },
  { city: "JAUHRABAD", value: "441" },
  { city: "JEHANGIRA", value: "646" },
  { city: "JHANG", value: "40" },
  { city: "JHANG SADAR", value: "588" },
  { city: "JHANGI WALA", value: "703" },
  { city: "JHAWRIAN", value: "587" },
  { city: "Jhelum", value: "210" },
  { city: "JHUDDO", value: "735" },
  { city: "Johar Town", value: "130" },
  { city: "Joharabad", value: "236" },
  { city: "Johi", value: "265" },
  { city: "JUND WALA", value: "673" },
  { city: "KABIR WALA", value: "476" },
  { city: "Kabirwala", value: "312" },
  { city: "Kacha Khoou", value: "680" },
  { city: "Kacha Khu", value: "160" },
  { city: "Kahna Nau", value: "294" },
  { city: "Kahuta", value: "222" },
  { city: "KALA", value: "666" },
  { city: "Kala Bagh", value: "335" },
  { city: "Kalar Syedan", value: "221" },
  { city: "KALLAR KAHAR", value: "638" },
  { city: "KALLAR SYEDAN", value: "625" },
  { city: "Kallur Kot", value: "227" },
  { city: "Kamalia", value: "232" },
  { city: "Kamar Mashani", value: "226" },
  { city: "Kamber", value: "261" },
  { city: "Kameer", value: "360" },
  { city: "KAMMAR MUSHANI", value: "453" },
  { city: "KAMO SHAHEED", value: "706" },
  { city: "Kamoke", value: "315" },
  { city: "KAMOKI", value: "553" },
  { city: "Kamra", value: "324" },
  { city: "Kandh Kot", value: "57" },
  { city: "Kandhkot", value: "247" },
  { city: "Kandiaro", value: "351" },
  { city: "KANJROR", value: "573" },
  { city: "KANJU", value: "469" },
  { city: "KANJWANI STATION", value: "445" },
  { city: "KARACHI", value: "64" },
  { city: "Karak", value: "201" },
  { city: "Karianwala", value: "168" },
  { city: "KARONDI", value: "734" },
  { city: "Karor Lal Ehsan", value: "481" },
  { city: "Karor Lal Esan", value: "667" },
  { city: "Karor Lalesan", value: "157" },
  { city: "Kashmore", value: "251" },
  { city: "Kasur", value: "127" },
  { city: "KATHIALA SHEIKHAN", value: "546" },
  { city: "Kehror Pakka", value: "71" },
  { city: "KEHRORPACCA", value: "400" },
  { city: "KELASKEY", value: "575" },
  { city: "Kemari", value: "284" },
  { city: "KERIANWALA", value: "368" },
  { city: "Khairpur", value: "273" },
  { city: "Khairpur", value: "62" },
  { city: "Khairpur Nathan Shah", value: "262" },
  { city: "Khairpur Sadat", value: "676" },
  { city: "KHALAOW", value: "653" },
  { city: "Khan Bela", value: "346" },
  { city: "Khan Gah Sharif", value: "93" },
  { city: "Khan Garh", value: "308" },
  { city: "Khanewal", value: "161" },
  { city: "KHANKAHDOGRAN", value: "536" },
  { city: "Khanpur", value: "95" },
  { city: "Khanpur Mahar", value: "252" },
  { city: "Khanqa Dogran", value: "178" },
  { city: "Kharian", value: "169" },
  { city: "Khichiwala", value: "138" },
  { city: "KHIDARWALA", value: "442" },
  { city: "Khipro", value: "112" },
  { city: "KHOI RATTA (A.K)", value: "635" },
  { city: "KHOSKI", value: "723" },
  { city: "Khudian", value: "518" },
  { city: "Khudian Khas", value: "357" },
  { city: "KHURIANWALA", value: "38" },
  { city: "Khuriratta", value: "211" },
  { city: "Khurrianwala", value: "89" },
  { city: "Khushab", value: "91" },
  { city: "Khuzdar", value: "281" },
  { city: "KINGRA MORE", value: "545" },
  { city: "Kohat", value: "325" },
  { city: "Koral Ghati", value: "124" },
  { city: "Koror Lal Esan", value: "682" },
  { city: "Kot Addu", value: "50" },
  { city: "KOT BANGLO", value: "736" },
  { city: "KOT BHADAR SHAH", value: "602" },
  { city: "KOT CHANDNA", value: "452" },
  { city: "Kot Chutta", value: "303" },
  { city: "KOT JAIMAL (A.K)", value: "622" },
  { city: "KOT KARAM KHAN", value: "712" },
  { city: "KOT MITHON", value: "474" },
  { city: "KOT MOMIN", value: "444" },
  { city: "Kot Radha Kishan", value: "362" },
  { city: "Kot Sabzal", value: "249" },
  { city: "Kot Sultan", value: "158" },
  { city: "Kotaddu", value: "150" },
  { city: "Kotla Arab Ali Khan", value: "170" },
  { city: "KOTLALO", value: "742" },
  { city: "Kotli", value: "85" },
  { city: "KOTLI (A.K)", value: "464" },
  { city: "KOTLI LOHARAN", value: "532" },
  { city: "KOTLI MORR", value: "669" },
  { city: "KOTRI", value: "732" },
  { city: "KUD WALA", value: "699" },
  { city: "KUNDIAN", value: "596" },
  { city: "KUNJAH", value: "435" },
  { city: "Kunri", value: "254" },
  { city: "LAB MAIL", value: "385" },
  { city: "Lahore", value: "1" },
  { city: "LAISER KLAN", value: "552" },
  { city: "Lakki Marwat", value: "202" },
  { city: "Lal Sohanra", value: "240" },
  { city: "Lalamusa", value: "7" },
  { city: "Lalian", value: "237" },
  { city: "LALIYYAN", value: "438" },
  { city: "Lalmusa", value: "9" },
  { city: "Larkana", value: "63" },
  { city: "Lasbaila", value: "747" },
  { city: "Layyah", value: "51" },
  { city: "LIAQTPUR", value: "495" },
  { city: "Liaquatpur", value: "96" },
  { city: "LODHARAN", value: "396" },
  { city: "Lodhran", value: "139" },
  { city: "Loralai", value: "353" },
  { city: "LUCKY MARWAT", value: "645" },
  { city: "Luddan", value: "162" },
  { city: "Lyari", value: "283" },
  { city: "M PUR DEWAN", value: "484" },
  { city: "M PUR LAMMA", value: "696" },
  { city: "M.Garh", value: "309" },
  { city: "MACHIWAL", value: "473" },
  { city: "MACHKA NOOR SHAH", value: "716" },
  { city: "MADI SHAH JIVNA", value: "608" },
  { city: "MAHRE WALA", value: "689" },
  { city: "Mailsi", value: "313" },
  { city: "MAKHDOM RASHEED", value: "671" },
  { city: "Makin", value: "206" },
  { city: "Malakwal", value: "171" },
  { city: "MAMU KANJAN", value: "606" },
  { city: "Manawala", value: "321" },
  { city: "MANDANI", value: "644" },
  { city: "Mandi Bahauddin", value: "172" },
  { city: "MANDI BAHAUDIN", value: "371" },
  { city: "MANDI FAIZABAD", value: "427" },
  { city: "MANDI MEDRASSA", value: "661" },
  { city: "MANDI SUKHEKI", value: "577" },
  { city: "MANDRA", value: "629" },
  { city: "Manga Mandi", value: "355" },
  { city: "Mangowal", value: "175" },
  { city: "MANKIALA STATION", value: "462" },
  { city: "MANSEHRA", value: "20" },
  { city: "Mardan", value: "216" },
  { city: "MARGAI", value: "688" },
  { city: "MARROT", value: "663" },
  { city: "MARROTT", value: "488" },
  { city: "Matli", value: "258" },
  { city: "MATTA", value: "626" },
  { city: "Mcleod Ganj", value: "300" },
  { city: "MECLOD GANJ", value: "687" },
  { city: "Meer Hazar", value: "151" },
  { city: "Mehar", value: "107" },
  { city: "Mehrabpur", value: "116" },
  { city: "Mian Channu", value: "46" },
  { city: "MIAN WALI BANGLA", value: "576" },
  { city: "MIANWALI", value: "43" },
  { city: "MIANWALI QURASHIAN", value: "695" },
  { city: "MINCHANABAD", value: "399" },
  { city: "Mir Pur Sakhro", value: "508" },
  { city: "Mirpur", value: "212" },
  { city: "MIRPUR (A.K)", value: "620" },
  { city: "Mirpur Bhatoro", value: "744" },
  { city: "Mirpur khas", value: "255" },
  { city: "Mirpur Mathelo", value: "56" },
  { city: "Mirpur Sakro", value: "287" },
  { city: "MIRPURKHAS", value: "410" },
  { city: "MIRWAH GORCHANI", value: "409" },
  { city: "Mithi", value: "104" },
  { city: "MONGI BANGLA", value: "585" },
  { city: "MOR BHUTTY WAHAN", value: "493" },
  { city: "MORE AIMANABAD", value: "555" },
  { city: "MORE KHUNDA", value: "433" },
  { city: "MORE SAMBRIAL", value: "558" },
  { city: "Moro", value: "266" },
  { city: "MOTRA", value: "365" },
  { city: "MOZA BAGH", value: "604" },
  { city: "MUBARAK PUR", value: "710" },
  { city: "Muhammad Pur Dewan", value: "145" },
  { city: "Multan", value: "44" },
  { city: "Multan (Lar)", value: "148" },
  { city: "Mureed kay", value: "179" },
  { city: "Mureed Shakh", value: "250" },
  { city: "MUREEDWALA", value: "436" },
  { city: "MURIDKY", value: "570" },
  { city: "Musafir Khana", value: "94" },
  { city: "Mustafabad", value: "125" },
  { city: "MUZAFFARABAD", value: "24" },
  { city: "MUZAFFARABAD (A.K)", value: "460" },
  { city: "Muzaffargarh", value: "402" },
  { city: "Nai wala Bangla", value: "513" },
  { city: "Nankana", value: "79" },
  { city: "NANKANA SAHIB", value: "374" },
  { city: "Narang Mandi", value: "186" },
  { city: "Narowal", value: "6" },
  { city: "NAUDERO", value: "504" },
  { city: "Naushero Feroz", value: "274" },
  { city: "NAUSHERO FEROZE", value: "416" },
  { city: "NAWA LAHORE", value: "449" },
  { city: "NAWAB WALI MUHAMMAD", value: "726" },
  { city: "Nawabshah", value: "267" },
  { city: "Nokhar", value: "166" },
  { city: "NOKHAR MANDI", value: "566" },
  { city: "NONAR", value: "542" },
  { city: "NOOR PUR NORANGA", value: "491" },
  { city: "NOOR PUR THAL", value: "605" },
  { city: "Nooriabad", value: "288" },
  { city: "Noorkot", value: "180" },
  { city: "NOSHEHRA", value: "586" },
  { city: "Noshera  Virkan", value: "316" },
  { city: "NOSHERA VIRKAN", value: "533" },
  { city: "Nowshera", value: "330" },
  { city: "OBAARO", value: "492" },
  { city: "Obaro", value: "248" },
  { city: "Oghi", value: "198" },
  { city: "Okara", value: "3" },
  { city: "P.D. Khan", value: "86" },
  { city: "P.D.KHAN", value: "627" },
  { city: "PADEDIN", value: "727" },
  { city: "Paharpur", value: "81" },
  { city: "PAHERANWALI", value: "372" },
  { city: "PAINSRA", value: "448" },
  { city: "Pakpatan", value: "135" },
  { city: "PakPattan Sharif", value: "421" },
  { city: "PALANDRI (A.K)", value: "637" },
  { city: "Panjgur", value: "282" },
  { city: "PANO AQIL", value: "500" },
  { city: "Panu Aqil", value: "350" },
  { city: "Parachinar", value: "205" },
  { city: "Pasroor", value: "193" },
  { city: "PASROR", value: "434" },
  { city: "Pattoki", value: "128" },
  { city: "PEERO CHAK", value: "432" },
  { city: "PESHAWAR", value: "16" },
  { city: "Phalia", value: "173" },
  { city: "PHARIANWALI", value: "567" },
  { city: "Phool Nagar", value: "519" },
  { city: "Phoolnagar", value: "129" },
  { city: "PHULARWAN", value: "594" },
  { city: "PHULJI", value: "738" },
  { city: "PIND SULTANI", value: "630" },
  { city: "Pindi Bhatian", value: "78" },
  { city: "Pindi Bhattian", value: "167" },
  { city: "PINDI BOHRI", value: "538" },
  { city: "PINDI GHEP", value: "640" },
  { city: "PINDI SAID PUR", value: "636" },
  { city: "Piplan", value: "228" },
  { city: "PIR BABA", value: "26" },
  { city: "PIR JO GOTH", value: "502" },
  { city: "Pir Mahal", value: "339" },
  { city: "PIRMAHAL", value: "583" },
  { city: "Pishin", value: "291" },
  { city: "PULL LAXIAN", value: "579" },
  { city: "Q.D.Singh", value: "165" },
  { city: "Qaboola Sharif", value: "528" },
  { city: "QADIRABAD", value: "574" },
  { city: "QADIRPUR", value: "701" },
  { city: "QAIM BHARWANA", value: "591" },
  { city: "Qaimpur", value: "243" },
  { city: "Qasba Gujrat", value: "152" },
  { city: "Qazi Ahmed", value: "268" },
  { city: "QILA AHMAD ABAD", value: "429" },
  { city: "Qila Ahmed Abad", value: "181" },
  { city: "QILA DIDAR SINGH", value: "547" },
  { city: "QILA KALAR WALA", value: "375" },
  { city: "Qilla Saifullah", value: "122" },
  { city: "Quaidabad", value: "342" },
  { city: "QUETTA", value: "66" },
  { city: "QUTUB PUR", value: "659" },
  { city: "RABWA", value: "611" },
  { city: "Rahim Yar Khan", value: "97" },
  { city: "RAHWALI CANTT", value: "369" },
  { city: "Raiwind", value: "295" },
  { city: "RAIWND", value: "418" },
  { city: "RAJA RAM", value: "670" },
  { city: "Rajan Pur", value: "304" },
  { city: "Rajana", value: "340" },
  { city: "Rajanpur", value: "49" },
  { city: "Rajo Wal", value: "524" },
  { city: "RAJOO KHANANI", value: "740" },
  { city: "Rana town", value: "187" },
  { city: "RangPur", value: "203" },
  { city: "Ranipur", value: "275" },
  { city: "Rawalakot", value: "223" },
  { city: "RAWALAKOT (A.K)", value: "391" },
  { city: "RAWALPINDI", value: "14" },
  { city: "Renala Khurd", value: "359" },
  { city: "Renalakhurd", value: "136" },
  { city: "RISALPUR", value: "459" },
  { city: "RODU SULTAN", value: "439" },
  { city: "Rohilanwali", value: "153" },
  { city: "Rohri", value: "276" },
  { city: "Rojhan", value: "141" },
  { city: "RUKAN PUR", value: "721" },
  { city: "RUSTOM", value: "499" },
  { city: "Sadar Gogera", value: "361" },
  { city: "Saddar", value: "285" },
  { city: "Sadiqabad", value: "55" },
  { city: "SAFDARABAD", value: "562" },
  { city: "Sahiwal", value: "68" },
  { city: "Sajawal", value: "289" },
  { city: "Sakhi Sarwar", value: "142" },
  { city: "Sakrand", value: "269" },
  { city: "Saleh Pat", value: "277" },
  { city: "SAMARO", value: "729" },
  { city: "Sambrial", value: "192" },
  { city: "Sammundri", value: "337" },
  { city: "SAMUNDRI", value: "446" },
  { city: "Sanawan", value: "655" },
  { city: "SANDHILAINWALI", value: "603" },
  { city: "Sanghar", value: "60" },
  { city: "Sanghar", value: "113" },
  { city: "Sangla Hill", value: "322" },
  { city: "SANKHATRA", value: "549" },
  { city: "Sarai Alamgir", value: "174" },
  { city: "SARDAR GARH", value: "708" },
  { city: "Sardar Ghar", value: "98" },
  { city: "SARGODHA", value: "39" },
  { city: "SARHAD", value: "702" },
  { city: "Sarhari", value: "114" },
  { city: "Satiana", value: "230" },
  { city: "SATYANA", value: "580" },
  { city: "Sawabi", value: "331" },
  { city: "SEHJA", value: "697" },
  { city: "SEHNSA (A.K)", value: "387" },
  { city: "SEHWAN", value: "408" },
  { city: "Sehwan Sharif", value: "115" },
  { city: "Serai Saleh", value: "199" },
  { city: "SHAB QADAR", value: "618" },
  { city: "Shadan Lund", value: "143" },
  { city: "SHADIA", value: "581" },
  { city: "SHADIWAL", value: "370" },
  { city: "Shah Faisal", value: "290" },
  { city: "Shah Jamal", value: "489" },
  { city: "SHAH KOT", value: "568" },
  { city: "Shahdadkot", value: "108" },
  { city: "Shahdadpur", value: "270" },
  { city: "Shahjamal", value: "154" },
  { city: "Shahkot", value: "182" },
  { city: "SHAHPUR CHAKAR", value: "413" },
  { city: "SHAKAR GHARH", value: "431" },
  { city: "Shakargarh", value: "323" },
  { city: "Sharqpur", value: "183" },
  { city: "SHARQPUR SHARIF", value: "571" },
  { city: "Shehr Sultan", value: "155" },
  { city: "Sheikhupura", value: "184" },
  { city: "Sher Garh", value: "525" },
  { city: "SHER GHAR", value: "18" },
  { city: "SHERIN KOTHE", value: "461" },
  { city: "SHEWA KALAY", value: "455" },
  { city: "Shikarpur", value: "263" },
  { city: "Shiva adda", value: "217" },
  { city: "Shorkot", value: "207" },
  { city: "Shujabad", value: "75" },
  { city: "SIAL MORE", value: "613" },
  { city: "Sialkot", value: "5" },
  { city: "SIALKOT", value: "428" },
  { city: "Sibbi", value: "354" },
  { city: "SIKANDARABAD", value: "600" },
  { city: "Sillanwali", value: "343" },
  { city: "Sinjavi", value: "123" },
  { city: "SKARDU", value: "23" },
  { city: "SMUNDRI", value: "376" },
  { city: "Soay Asal", value: "510" },
  { city: "SOBO DERO", value: "722" },
  { city: "Sohawa", value: "213" },
  { city: "SUI", value: "711" },
  { city: "SUKHO", value: "642" },
  { city: "Sukkur", value: "61" },
  { city: "Sukkur", value: "117" },
  { city: "SULEMAN NAGAR", value: "619" },
  { city: "Swabi", value: "34" },
  { city: "SWARI (BUNER)", value: "463" },
  { city: "Swari-Buner", value: "218" },
  { city: "Swat", value: "332" },
  { city: "T.T SING", value: "42" },
  { city: "Tail Indus", value: "483" },
  { city: "TAKHT-E-NUSRATI", value: "389" },
  { city: "TALAGANG", value: "12" },
  { city: "TALASH", value: "652" },
  { city: "Tanda", value: "320" },
  { city: "TANDLIANWALA", value: "37" },
  { city: "Tando Adam", value: "271" },
  { city: "Tando Allah Yar", value: "256" },
  { city: "TANDO ALLAHYAR", value: "724" },
  { city: "TANDO JAM", value: "505" },
  { city: "TANDO JAN MUHAMMAD", value: "741" },
  { city: "Tando Mohammad Khan", value: "257" },
  { city: "TANDO MUHAMMAD KHAN", value: "407" },
  { city: "TANDO RAHIM KHAN", value: "417" },
  { city: "Tangwani", value: "101" },
  { city: "Tank", value: "82" },
  { city: "TARANDA M PANAH", value: "698" },
  { city: "TARANDA SEWAE KHAN", value: "494" },
  { city: "Tarranda Muhammad Pannah", value: "244" },
  { city: "Taunsa", value: "74" },
  { city: "TAXILA", value: "25" },
  { city: "Thall", value: "204" },
  { city: "THANA", value: "633" },
  { city: "Thari Mirwah", value: "118" },
  { city: "Thatta", value: "121" },
  { city: "THEEKRI WALA", value: "447" },
  { city: "Thehri", value: "278" },
  { city: "Thingi", value: "163" },
  { city: "Thull", value: "109" },
  { city: "Timargarh", value: "333" },
  { city: "TOBA TAK SINGH", value: "379" },
  { city: "Toba Tek Singh", value: "233" },
  { city: "TOPI", value: "33" },
  { city: "TRAHIM YAR KHAN", value: "405" },
  { city: "Turbat", value: "119" },
  { city: "UCH SHARIEF", value: "700" },
  { city: "Uch Sharif", value: "52" },
  { city: "UGGOKI", value: "548" },
  { city: "Umer Kot", value: "348" },
  { city: "Usta Mohammad", value: "110" },
  { city: "Utha", value: "745" },
  { city: "Uthal", value: "507" },
  { city: "Veahri", value: "672" },
  { city: "Vehari", value: "45" },
  { city: "VERYAM", value: "541" },
  { city: "Vindhar", value: "120" },
  { city: "WAH CANTT", value: "13" },
  { city: "WANG", value: "482" },
  { city: "WARBURTON", value: "537" },
  { city: "Wazirabad", value: "317" },
  { city: "Whijianwala", value: "472" },
  { city: "Winder", value: "509" },
  { city: "Yar Hussain", value: "219" },
  { city: "Yazman", value: "345" },
  { city: "Zafarwal", value: "185" },
  { city: "ZAHIR PIR", value: "719" },
  { city: "Zahirpir", value: "99" },
  { city: "ZAREEF SHAHEED", value: "690" },
  { city: "Zhob", value: "292" },
];
module.exports = cities;