const fetch = require("node-fetch");

const url = "https://graphql.automark.pk/v1/graphql";
const graphbody = (
  address,
  contact_person,
  name,
  phone_number,
  city
) => `mutation MyMutation {
  insert_dealer_one(object: {
    address: "${address}", 
    contact_person: "${contact_person}", 
    name: "${name}", 
    phone_number: "${phone_number}",
    city: "${city}"
  }) {
    id
  }
}`;

const graphCity = (city) => `mutation AddCity {
  insert_city(objects: {name: "${city}"}) {
    returning {
      name
    }
  }
}`;

const cities = require("./cities");
(async () => {
  let fiveGroup = [];
  for (let i = 0; i < cities.slice(0, 10).length; i++) {
    const cityId = cities[i].value;

    const params = new URLSearchParams();
    params.append("jx", 1);
    params.append("cty", parseInt(cityId));
    params.append("type", "d");

    const res = fetch(
      "https://www.atlashonda.com.pk/wp-content/plugins/gmaps/main2.php",
      { method: "POST", body: params }
    );
    
    fiveGroup.push(res);
    
    if (fiveGroup.length === 5) {
      const results = await Promise.all(fiveGroup);
      results.map(result => {
        const t = await result.text();
        const $ = cheerio.load(t);
        
      });
      fiveGroup = [];
    }
    // const cityName = cities[i].city;
    // const n = cityName.charAt(0).toUpperCase() + cityName.slice(1).toLowerCase();
    // const cityRequest = await fetch(url, {
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   method: 'post',
    //   body: JSON.stringify({
    //     query: graphCity(n)
    //   })
    // });
    // console.log((await cityRequest.json()).data.insert_city.returning[0]);
  }
})();
