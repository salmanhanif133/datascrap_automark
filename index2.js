const fs = require("fs");
const fetch = require("node-fetch");
const cheerio = require("cheerio");

let dataArray = require("./output/array.js");

const { URLSearchParams } = require("url");
const { type } = require("os");
const url = 'https://graphql.automark.pk/v1/graphql';
const graphbody = (address, contact_person, name, phone_number, city) => `mutation MyMutation {
  insert_dealer_one(object: {
    address: "${address}", 
    contact_person: "${contact_person}", 
    name: "${name}", 
    phone_number: "${phone_number}",
    city: "${city}"
  }) {
    id
  }
}`;
let i = 1;

let errorCities = [];
(async () => {
  for (let i = 0; i < dataArray.array.length; i++) {
    const data = dataArray.array[i];
    console.log(`${i++} and  ${parseInt(data.value)}`);
    const params = new URLSearchParams();
    params.append("jx", 1);
    params.append("cty", parseInt(data.value));
    params.append("type", "d");

    let dealersArray = [];

    let body = null;
    try {
      const res = await fetch(
        "https://www.atlashonda.com.pk/wp-content/plugins/gmaps/main2.php",
        { method: "POST", body: params }
      );
      console.log(res.status);
      if (res.status !== 200) {
        errorCities.push(data);
        fs.writeFileSync(
          "./output/error.json",
          JSON.stringify(errorCities, null, 2),
          "utf8"
        );
        break;
      }
      body = await res.text();
    } catch (e) {
      console.log(e);
      break;
    }
    if (!body) {
      break;
    }

    const $ = cheerio.load(body);
    console.log($("#error-page").find("div").text());
    let dealer = $("div").find(".store-title").text();
    let address = $("div").find(".store-address").text();
    let number = `${$("div").find(".store-phone").first().text()} ${$("div").find(".store-phone").last().text()}`;
    let city = data.city

    console.log(`${address}`)
    // const postRequest = await fetch(url, {
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   method: 'post',
    //   body: JSON.stringify({
    //     query: graphbody(address, name, dealer, number, city)
    //   })
    // });

    // if (postRequest.status === 200) {
    //   console.log("OK")
    // } else {
    //   console.log("Error")
    // }

  }
})();
